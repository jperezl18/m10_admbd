--Jorge D. Pérez López
--a232508jp


--Funció mostraEmpleats amb bulce LOOP:
/*
CREATE OR REPLACE FUNCTION mostraEmpleats (
	p_job VARCHAR(100)
	) RETURNS VARCHAR

AS $$

DECLARE

	emp_cursor CURSOR FOR
		SELECT ename,job
		FROM emp
		WHERE LOWER(job) = LOWER(p_job);

	emp_record RECORD;
	v_msg VARCHAR(500);

BEGIN

	v_msg := '';

	OPEN emp_cursor;

	LOOP
		FETCH NEXT FROM emp_cursor INTO emp_record;
		EXIT WHEN NOT FOUND;

		v_msg := v_msg || emp_record.ename || ' ' || emp_record.job || chr(10);

	END LOOP;

	CLOSE emp_cursor;

	RETURN v_msg;

END;
$$ LANGUAGE plpgsql;
*/

--Funció mostraEmpleats amb bucle WHILE:
/*
CREATE OR REPLACE FUNCTION mostraEmpleats (
        p_job VARCHAR(100)
        ) RETURNS VARCHAR

AS $$

DECLARE

        emp_cursor CURSOR FOR
                SELECT ename,job
                FROM emp
                WHERE LOWER(job) = LOWER(p_job);

        emp_record RECORD;
        v_msg VARCHAR(500);

BEGIN

        v_msg := '';

        OPEN emp_cursor;

	FETCH NEXT FROM emp_cursor INTO emp_record;

        WHILE FOUND  
	LOOP
        	v_msg := v_msg || emp_record.ename || ' ' || emp_record.job || chr(10);
		FETCH NEXT FROM emp_cursor INTO emp_record;

        END LOOP;

        CLOSE emp_cursor;

        RETURN v_msg;

END;
$$ LANGUAGE plpgsql;
*/

--Funció mostraEmpleats amb bucle FOR:
CREATE OR REPLACE FUNCTION mostraEmpleats (
        p_job VARCHAR(100)
        ) RETURNS VARCHAR

AS $$

DECLARE

        emp_cursor CURSOR FOR
                SELECT ename,job
                FROM emp
                WHERE LOWER(job) = LOWER(p_job);

        v_msg VARCHAR(500);

BEGIN

        v_msg := '';

	FOR emp_record IN emp_cursor
        LOOP
                v_msg := v_msg || emp_record.ename || ' ' || emp_record.job || chr(10);

        END LOOP;

	RETURN v_msg;

END;
$$ LANGUAGE plpgsql;
