--Jorge D. Pérez López
--a232508jp

\c template1
DROP DATABASE IF EXISTS bdfuncions;
CREATE DATABASE bdfuncions;
\c bdfuncions

--textPla
CREATE OR REPLACE FUNCTION textPla (
	p_cadena VARCHAR(100)
	) RETURNS VARCHAR(100)

AS $$

DECLARE
	v_textTransformat VARCHAR(100);

BEGIN

	SELECT translate(p_cadena, 
		         'àáäÀÁÄèéëÈÉËìíïÌÍÏòóöÒÓÖùúüÙÚÜñÑçÇ', 
		         'aaaAAAeeeEEEiiiIIIoooOOOuuuUUUnNcC')
	INTO v_textTransformat;
	RETURN v_textTransformat;

END;

$$ LANGUAGE plpgsql;

--validaDNI

CREATE OR REPLACE FUNCTION validaDNI (
	p_dni VARCHAR(9)
        ) RETURNS VARCHAR(50)

AS $$

DECLARE

	missatge VARCHAR(50);
	v_lletra VARCHAR(50);
	v_lletres VARCHAR(23);
	v_numeros INT;
	v_modul INT;

BEGIN

	v_lletra := UPPER(substr(p_dni,9,1));
	v_numeros := substr(p_dni,1,8);
	v_lletres := 'TRWAGMYFPDXBNJZSQVHLCKE';

	v_modul := (v_numeros % 23) + 1;

	IF v_lletra = substr(v_lletres,v_modul,1)
	THEN
		missatge := p_dni || ' correcte';
	ELSE
		missatge := p_dni || ' incorrecte';
	END IF;

	RETURN missatge;

END;

$$ LANGUAGE plpgsql;

\c training

/* 3.a. Per a cada representant, mostrar el camp  nombre 
i la posició on comença el cognom.

SELECT nombre, position(' ' in nombre) 
FROM repventa;
*/

/* 3.b. Per cada representant, mostra el el camp nombre 
(tal com està a la base de dades) i la columna Apellido

SELECT substr(nombre,1,position(' ' in nombre)) nombre, 
	   substr(nombre,position(' ' in nombre),10) apellido
FROM repventa;
*/

/* 3.c. Per cada representant, mostra el camp nombre i el seu login.

SELECT substr(nombre,1,position(' ' in nombre)) nombre,
	   LOWER(substr(nombre,1,1)) || LOWER(substr(nombre,position(' ' in nombre)+1,7)) login
FROM repventa;
*/

/* 3.d. A partir del codi implementat, escriu la sentència (només 1!!) en SQL per crear la taula repventa2. 
Evidentment començarà per "create table". Utilitzeu la funció d'usuari textPla del primer exercici. */

CREATE OR REPLACE FUNCTION textPla (
	p_cadena VARCHAR(100)
	) RETURNS VARCHAR(100)

AS $$

DECLARE
	v_textTransformat VARCHAR(100);

BEGIN

	SELECT translate(p_cadena, 
		         'àáäÀÁÄèéëÈÉËìíïÌÍÏòóöÒÓÖùúüÙÚÜñÑçÇ', 
		         'aaaAAAeeeEEEiiiIIIoooOOOuuuUUUnNcC')
	INTO v_textTransformat;
	RETURN v_textTransformat;

END;

$$ LANGUAGE plpgsql;

CREATE TABLE repventa2 AS SELECT *,
	textPla(LOWER(substr(nombre,1,1)) || LOWER(substr(nombre,position(' ' in nombre)+1,7))) login
FROM repventa;



