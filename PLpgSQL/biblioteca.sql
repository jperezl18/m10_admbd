--Jorge D. Pérez López
--a232508jp

\c biblioteca

--codiExemplarDisponible

CREATE OR REPLACE FUNCTION codiExemplarDisponible (
    p_titol VARCHAR(200),
    p_format VARCHAR(100)
    ) RETURNS INT

AS $$

DECLARE

    disponibles_record RECORD;
    disponibles_cursor CURSOR FOR
    	SELECT e.idexemplar
    	FROM prestec p
            JOIN exemplar e ON p.idexemplar=e.idexemplar
            JOIN document d ON e.iddocument=d.iddocument
	WHERE LOWER(estat) = 'disponible' AND datadev IS NOT NULL AND LOWER(titol) = LOWER(p_titol) AND LOWER(format) = LOWER(p_format);

BEGIN

    OPEN disponibles_cursor;

    FETCH NEXT FROM disponibles_cursor INTO disponibles_record;

    CLOSE disponibles_cursor;

    IF disponibles_record IS NOT NULL THEN
	RETURN disponibles_record.idexemplar;
    ELSE
	RETURN 0;
    END IF;
    
END;
$$ LANGUAGE plpgsql;

--usuariOK

CREATE OR REPLACE FUNCTION usuariOK (
	p_usuari INT
 	) RETURNS SMALLINT

AS $$

DECLARE

	v_sortida SMALLINT;
	v_bloquejat BOOLEAN;

BEGIN
	
	v_sortida := 2;

	SELECT bloquejat
	INTO STRICT v_bloquejat
	FROM usuari
	WHERE idUsuari = p_usuari;

	IF v_bloquejat = 't' THEN
		v_sortida := 1;
	ELSE
		v_sortida := 0;
	END IF;

	RETURN v_sortida;

	EXCEPTION
                WHEN NO_DATA_FOUND THEN
                        RETURN v_sortida;

END;
$$ LANGUAGE plpgsql;

--documentsPrestats

CREATE OR REPLACE FUNCTION documentsPrestats (
	p_usuari INT,
	p_format VARCHAR(100)
 	) RETURNS SMALLINT

AS $$

DECLARE

	v_numDocuments SMALLINT;

BEGIN

	SELECT COUNT (p.idExemplar)
	INTO v_numDocuments
	FROM prestec p 
		JOIN exemplar e ON p.idExemplar=e.idExemplar
		JOIN document d ON e.idDocument=d.idDocument
	WHERE p.idUsuari = p_usuari AND d.format = p_format AND datadev IS NULL;
	RETURN v_numDocuments;
	
END;
$$ LANGUAGE plpgsql;

--prestecDocument

CREATE OR REPLACE FUNCTION prestecDocument (
	p_idUsuari INT,
	p_titol VARCHAR(200),
	p_format VARCHAR(100)
 	) RETURNS VARCHAR(100)

AS $$

DECLARE

	missatge VARCHAR(100);
	v_limitFormat SMALLINT;

BEGIN

	IF p_format = 'llibre' OR p_format = 'revista' THEN
		v_limitFormat := 2;
	ELSIF p_format = 'CD' OR p_format = 'DVD' THEN
		v_limitFormat := 1;
	END IF;

	IF codiExemplarDisponible(p_titol)::INT = 0 THEN
		missatge := 'Error: L''Exemplar no està disponible.';
	ELSIF usuariOK(p_idUsuari) != 0 THEN
		missatge := 'Error: L''usuari no existeix o està bloquejat.';
	ELSIF documentsPrestats(p_idUsuari,p_format)::SMALLINT > v_limitFormat THEN
		missatge := 'Error: L''Usuari execeix el limit de prèstecs.';
	ELSE
		INSERT INTO prestec (idExemplar, datapres, datadev, idUsuari)
		VALUES (codiExemplarDisponible(p_titol), CURRENT_TIMESTAMP, null, p_idUsuari);
		missatge := 'Prèstec realitzat correctament.';
	END IF;

	RETURN missatge;

END;
$$ LANGUAGE plpgsql;

--retornarDocument

CREATE OR REPLACE FUNCTION retornarDocument (
	p_idExemplar INT
	) RETURNS VARCHAR(100)

AS $$

DECLARE 
	
	missatge VARCHAR(100);
	v_dataPres TIMESTAMP;
	v_idUsuari INT;
	v_diesPenal INT;
	v_punts INT;

BEGIN

	SELECT dataPres, idUsuari
	INTO STRICT v_dataPres, v_idUsuari
	FROM prestec
	WHERE idExemplar = p_idExemplar;

	v_diesPenal := TO_CHAR(CURRENT_TIMESTAMP - v_dataPres - '30 days', 'DD');

	IF v_diesPenal > 30 THEN
	        UPDATE usuari
	        SET punts = punts + v_diesPenal
 	        WHERE idUsuari = v_idUsuari;

		INSERT INTO penalitzacio	
		VALUES (v_idUsuari, p_idExemplar, CURRENT_DATE, v_diesPenal); 	
	END IF;	

	UPDATE prestec
	SET dataDev = CURRENT_TIMESTAMP
	WHERE idExemplar = p_idExemplar AND dataDev IS NULL;

	UPDATE exemplar
	SET estat = 'Disponible'
	WHERE idExemplar = p_idExemplar;

	SELECT punts
	INTO STRICT v_punts
	FROM usuari
	WHERE idUsuari = v_idUsuari;

	IF v_punts >= 50 THEN
		UPDATE usuari
		SET bloquejat = 't', punts = punts - 50
		WHERE idUsuari = v_idUsuari;
	END IF;	
	
	missatge := 'Document retornat.';

	RETURN missatge;

	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			missatge := 'Error: Exemplar no trobat.';
			RETURN missatge;

END;
$$ LANGUAGE plpgsql;

--reservarDocument

CREATE OR REPLACE FUNCTION reservarDocument (
	p_idUsuari INT,
	p_titol VARCHAR(200),
	p_format VARCHAR(100)
 	) RETURNS VARCHAR(100)

AS $$

DECLARE

	v_idExemplarDisponible INT;
	missatge VARCHAR(100);
	v_usuariOK INT;

BEGIN

	v_idExemplarDisponible := codiExemplarDisponible(p_titol,p_format);
	v_usuariOK := usuariOK(p_idUsuari);

	IF v_usuariOK = 1 THEN
		missatge := 'Error: L''usuari té el carnet bloquejat.';
	ELSIF v_usuariOK = 2 THEN
		missatge := 'Error: L''usuari no existeix';
	ELSE
		IF v_idExemplarDisponible = 0 THEN
			missatge := 'No hi ha cap exemplar disponible';
		ELSE
			INSERT INTO reserva
			VALUES (v_idExemplarDisponible, p_idUsuari, CURRENT_TIMESTAMP, null);
	
			missatge := 'Document reservat amb éxit.';
		END IF;
	END IF;

	RETURN missatge;

END;
$$ LANGUAGE plpgsql;

	