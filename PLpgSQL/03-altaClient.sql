--Jorge D. Pérez López
--a232508jp

CREATE SEQUENCE IF NOT EXISTS seq_cliente;

SELECT setval('seq_cliente', (select max(cliecod) from cliente), true);

CREATE OR REPLACE FUNCTION altaClient (p_nombre varchar(100), p_repcod smallint, p_limcred numeric(10,2))
returns varchar

as $$ 
  
  begin

    insert into cliente (cliecod, nombre, repcod, limcred)
    values (nextval('seq_cliente'), p_nombre, p_repcod, p_limcred);
    
    return 'Client ' || p_nombre || ' s''ha donat d''alta correctament.';

  end;

$$ LANGUAGE PLPGSQL;
    
