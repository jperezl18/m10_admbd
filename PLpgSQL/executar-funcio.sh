#! /bin/bash
# Jorge D. Pérez López
# 26/04/2024
#
# Sinopsis: executar-funcio.sh bbdd funció(paràmetres)
#
# Descripció: El programa rep el nom de la base de dades i el nom d'una funció i 
# l'executa al servidor de base de dades.
#---------------------------------------------------------------------------------
ERR_NARGS=1
ERR_BBDD=2
ERR_FUNC=3

if [ $# -ne 2 ]; then
  echo "Error: El programa ha de tenir dos arguments."
  echo "Usage: $0 bbdd funció(paràmetres)"
  exit $ERR_NARGS
fi

bbdd=$1
funcio=$2

psql template1 -q -t -c "SELECT datname FROM pg_database" | grep -q "$bbdd"

if [ $? -ne 0 ]; then
  echo "Error: La base de dades $bbdd no existeix."
  exit $ERR_BBDD
fi

onlyFuncio=$(echo "$funcio" | sed 's/([^)]*)//g')

psql "$bbdd" -q -t -c "\df" | grep -q "$onlyFuncio"

if [ $? -ne 0 ]; then
  echo "Error: La funció $onlyFuncio no existeix a la bbdd $bbdd o no està carregada."
  exit $ERR_FUNC
fi

psql "$bbdd" -t -c "SELECT $funcio"

exit 0
