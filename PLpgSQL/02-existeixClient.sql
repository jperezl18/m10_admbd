--Jorge D. Pérez López
--a232508jp

CREATE OR REPLACE FUNCTION existeixClient (p_cliecod SMALLINT)
RETURNS BOOLEAN
as $$
    declare
        client_existeix BOOLEAN;
    begin
        select count(*) > 0
        into strict client_existeix
        from cliente
        where cliecod=p_cliecod;
        return client_existeix;

        exception
	  when OTHERS then
             return false;
end;
    $$ LANGUAGE PLPGSQL;
