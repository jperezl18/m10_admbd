--Jorge D. Pérez López
--a232508jp

--afegirServei

CREATE OR REPLACE FUNCTION afegirServei (
    p_idServei SMALLINT,
    p_quantitat SMALLINT,
    p_numHab SMALLINT
    ) RETURNS VARCHAR(200)

AS $$

DECLARE

    missatge VARCHAR(200);
    v_descripcio VARCHAR(150);
    v_nom VARCHAR(40);
    v_preu NUMERIC(5,3);
    v_preuTotal NUMERIC(8,3);

BEGIN

    SELECT descripcio, preu
    INTO STRICT v_descripcio, v_preu
    FROM servei 
    WHERE idServei = p_idServei;

    v_preuTotal := v_preu * p_quantitat;

    SELECT nom 
    INTO STRICT v_nom
    FROM client c
        JOIN reserva r ON r.codClient = c.codClient
    WHERE numHab = p_numHab AND ocupada = 'S' AND facturada = 'N';

    INSERT INTO serveiHabitacio
    VALUES (p_numHab, p_idServei, CURRENT_TIMESTAMP, p_quantitat);

    missatge := v_nom || ', ' || p_numHab || ', ' || v_descripcio || ', ' || p_quantitat || ', Preu: ' || v_preu || ', Total: ' || v_preuTotal;

    RETURN missatge;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            missatge := 'Error: Paràmetres incorrectes.';
            RETURN missatge;

END;
$$ LANGUAGE plpgsql;

--checkIn

CREATE OR REPLACE FUNCTION checkIn (
    p_dni VARCHAR(9)
    ) RETURNS VARCHAR(200)

AS $$

DECLARE

    v_nom VARCHAR(40);
    v_diesEstancia SMALLINT;
    v_dataArribada DATE;
    v_dataSortida DATE;
    v_import NUMERIC(8,2);
    v_preuHab NUMERIC(5,2);
    missatge VARCHAR(200);
    v_numHab SMALLINT;
    v_numReserva SMALLINT;

BEGIN

    SELECT preu, dataArribada, dataSortida, nom, numReserva
    INTO STRICT v_preuHab, v_dataArribada, v_dataSortida, v_nom, v_numReserva
    FROM tipusHab th
        JOIN habitacio h ON h.codTipus = th.codTipus
        JOIN reserva r ON r.numHab = h.numHab
        JOIN client c ON r.codClient = c.codClient
    WHERE DNI = p_dni AND ocupada = 'N' AND facturada = 'N';

    v_diesEstancia := TO_CHAR(v_dataSortida - v_dataArribada, 'DD');
    v_import := v_preuHab * v_diesEstancia::INT;

    UPDATE reserva
    SET ocupada = 'S'
    WHERE numReserva = v_numReserva;

    INSERT INTO factura
    VALUES (NEXTVAL('seq_numfra'), v_numReserva, v_dataSortida, v_import);

    missatge := v_nom || ', ' || v_numHab;

    RETURN missatge;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            missatge := 'Error: No existeix el client.';
            RETURN missatge;

END;
$$ LANGUAGE plpgsql;