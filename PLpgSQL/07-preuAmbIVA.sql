--Jorge D. Pérez López
--a232508jp

CREATE OR REPLACE FUNCTION preuAmbIVA (
        p_precio NUMERIC(10,2) 
	) RETURNS NUMERIC

AS $$

DECLARE
        v_ambIVA NUMERIC(10,2);

BEGIN

        v_ambIVA := p_precio * 1.21;
        RETURN v_ambIVA;

END;

$$ LANGUAGE plpgsql;
