--Jorge D. Pérez López
--a232508jp

CREATE OR REPLACE FUNCTION stockOk(
    fabcod_p CHARACTER(3),
    prodcod_p CHARACTER(5),
    cant_p SMALLINT
) RETURNS BOOLEAN AS $$
DECLARE
    existencias_actuales INT;
BEGIN
    
    SELECT exist 
    INTO STRICT existencias_actuales
    FROM producto
    WHERE fabcod = fabcod_p AND prodcod = prodcod_p;
    RETURN existencias_actuales >= cant_p;

    EXCEPTION 
      WHEN NO_DATA_FOUND THEN 
        RETURN false;
    
END;
$$ LANGUAGE plpgsql;
