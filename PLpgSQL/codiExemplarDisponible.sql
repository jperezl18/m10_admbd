--Jorge D. Pérez López
--a232508jp

\c biblioteca

--Sense cursor:
/*
CREATE OR REPLACE FUNCTION codiExemplarDisponible (
    p_titol VARCHAR(200)
    ) RETURNS VARCHAR(50)

AS $$

DECLARE

    v_codiSortida INT;

BEGIN

    SELECT e.idexemplar
    INTO STRICT v_codiSortida
    FROM prestec p
        JOIN exemplar e ON p.idexemplar=e.idexemplar
        JOIN document d ON e.iddocument=d.iddocument
    WHERE LOWER(estat) = 'disponible' AND datadev IS NOT NULL AND LOWER(titol) = p_titol
    LIMIT 1;
    RETURN v_codiSortida;

    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            RETURN 0;

END;
$$ LANGUAGE plpgsql;
*/

--Amb cursor:
CREATE OR REPLACE FUNCTION codiExemplarDisponible (
    p_titol VARCHAR(200),
    p_format VARCHAR(100)
    ) RETURNS INT

AS $$

DECLARE

    disponibles_record RECORD;
    disponibles_cursor CURSOR FOR
    	SELECT e.idexemplar
    	FROM prestec p
            JOIN exemplar e ON p.idexemplar=e.idexemplar
            JOIN document d ON e.iddocument=d.iddocument
	WHERE LOWER(estat) = 'disponible' AND datadev IS NOT NULL AND LOWER(titol) = LOWER(p_titol) AND LOWER(format) = LOWER(p_format);

BEGIN

    OPEN disponibles_cursor;

    FETCH NEXT FROM disponibles_cursor INTO disponibles_record;

    CLOSE disponibles_cursor;

    IF disponibles_record IS NOT NULL THEN
	RETURN disponibles_record.idexemplar;
    ELSE
	RETURN 0;
    END IF;
    
END;
$$ LANGUAGE plpgsql;
    
