--Jorge D. Pérez López
--a232508jp

CREATE OR REPLACE FUNCTION preuSenseIVA (
	p_precio NUMERIC(10,2)
	) RETURNS NUMERIC

AS $$

DECLARE
	v_noIVA NUMERIC(10,2);

BEGIN

	v_noIVA := p_precio / 1.21;
	RETURN v_noIVA;

END;

$$ LANGUAGE plpgsql;

