--Jorge D. Pérez López
--a232508jp

CREATE OR REPLACE FUNCTION mostraUsuari (p_empno smallint)
returns varchar

as $$
  declare
    v_ename varchar(50);
    v_job varchar(50);

  begin

    select ename, job
    into strict v_ename, v_job
    from emp
    where empno = p_empno;
    
    return 'El nom de l''empleat amb codi ' || p_empno || ' és ' || v_ename || ' i treballa de ' || v_job; 

  end;

$$ LANGUAGE PLPGSQL;
    
