CREATE OR REPLACE FUNCTION prova(i INT)

RETURNS VARCHAR

AS $$

	BEGIN

		RETURN 'hello world' || i;
	END;

$$ LANGUAGE plpgsql;
