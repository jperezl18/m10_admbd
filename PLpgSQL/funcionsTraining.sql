--Jorge Daniel Pérez López
--a232508jp

--mostraUsuari.sql
CREATE OR REPLACE FUNCTION mostraUsuari (p_empno smallint)
returns varchar

as $$
  declare
    v_ename varchar(50);
    v_job varchar(50);

  begin

    select ename, job
    into strict v_ename, v_job
    from emp
    where empno = p_empno;
    
    return 'El nom de l''empleat amb codi ' || p_empno || ' és ' || v_ename || ' i treballa de ' || v_job; 

  end;

$$ LANGUAGE PLPGSQL;

--existeixClient.sql
CREATE OR REPLACE FUNCTION existeixClient (p_cliecod SMALLINT)
RETURNS BOOLEAN
as $$
    declare
        client_existeix BOOLEAN;
    begin
        select count(*) > 0
        into strict client_existeix
        from cliente
        where cliecod=p_cliecod;
        return client_existeix;

        exception
	  when OTHERS then
             return false;
end;
    $$ LANGUAGE PLPGSQL;

--altaClient.sql
CREATE SEQUENCE IF NOT EXISTS seq_cliente;

SELECT setval('seq_cliente', (select max(cliecod) from cliente), true);

CREATE OR REPLACE FUNCTION altaClient (p_nombre varchar(100), p_repcod smallint, p_limcred numeric(10,2))
returns varchar

as $$ 
  
  begin

    insert into cliente (cliecod, nombre, repcod, limcred)
    values (nextval('seq_cliente'), p_nombre, p_repcod, p_limcred);
    
    return 'Client ' || p_nombre || ' s''ha donat d''alta correctament.';

  end;

$$ LANGUAGE PLPGSQL;
    
--stockOk.sql
CREATE OR REPLACE FUNCTION stockOk(
    fabcod_p CHARACTER(3),
    prodcod_p CHARACTER(5),
    cant_p SMALLINT
) RETURNS BOOLEAN AS $$
DECLARE
    existencias_actuales INT;
BEGIN
    
    SELECT exist 
    INTO STRICT existencias_actuales
    FROM producto
    WHERE fabcod = fabcod_p AND prodcod = prodcod_p;
    RETURN existencias_actuales >= cant_p;

    EXCEPTION 
      WHEN NO_DATA_FOUND THEN 
        RETURN false;
    
END;
$$ LANGUAGE plpgsql;

--preuSenseIVA.sql

CREATE OR REPLACE FUNCTION preuSenseIVA (
	p_precio NUMERIC(10,2)
	) RETURNS NUMERIC

AS $$

DECLARE
	v_noIVA NUMERIC(10,2);

BEGIN

	v_noIVA := p_precio / 1.21;
	RETURN v_noIVA;

END;

$$ LANGUAGE plpgsql;

--preuAmbIVA.sql

CREATE OR REPLACE FUNCTION preuAmbIVA (
        p_precio NUMERIC(10,2) 
	) RETURNS NUMERIC

AS $$

DECLARE
        v_ambIVA NUMERIC(10,2);

BEGIN

        v_ambIVA := p_precio * 1.21;
        RETURN v_ambIVA;

END;

$$ LANGUAGE plpgsql;

