**Jorge D. Pérez López**  
**a232508jp**

# Funcions de transaccions (MONOUSUARI)

Creeu una base de dades anomenada __transaccions__, amb una única taula anomenada __punts__, amb la següent estructura: id de tipus INT (CP) i valor de tipus SMALLINT.

1. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  

```
INSERT INTO punts (id, valor) VALUES (10,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 10;
ROLLBACK;
SELECT valor FROM punts WHERE id = 10;
```  
Primer inicia una transacció no reversible amb INSERT.  
Després fa una transacció nova amb UPDATE que si serà reversible.  
Per últim restaura la transacció anterior.  

2. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.  
```
INSERT INTO punts (id, valor) VALUES (20,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 20;
COMMIT;
SELECT valor FROM punts WHERE id = 20;
```
Primer inicia una transacció no reversible amb INSERT.  
Després fa una transacció nova amb UPDATE que si serà reversible.  
Per últim fa definitiva la transacció anterior.  

3. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.

```
INSERT INTO punts (id, valor) VALUES (30,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 30;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (31,7);
ROLLBACK;
SELECT valor FROM punts WHERE id = 30;
```
Primer inicia una transacció no reversible amb INSERT.  
Després fa una transacció nova amb UPDATE que si serà reversible. Crea una savepoint anomenat "a".  
A continuació fa una altra transacció amb INSERT.  
Per últim restaura fins la primera transacció.  

4. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (40,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 40;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (41,7);
ROLLBACK TO a;
SELECT COUNT(*) FROM punts;
```
Primer inicia dos transaccions no reversible amb DELETE e INSERT.  
Després inicia una transacció nova amb UPDATE que si serà reversible. A continuació crea un savepoint anomenat "a".  
Fa una altra transacció amb INSERT.  
Per últim reverteix l'última transacció fent un rollback al savepoint.  

5. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
INSERT INTO punts (id, valor) VALUES (50,5);
BEGIN;
SELECT id, valor WHERE punts;
UPDATE punts SET valor = 4 WHERE id = 50;
COMMIT;
SELECT valor FROM punts WHERE id = 50;
```
Primer inicia una transacció no reversible amb INSERT.  
Després fa una transacció nova amb UPDATE que si serà reversible.  
Al executar el commit, farà un rollback ja que hi ha un error en el primer SELECT.  

6. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
```
DELETE FROM punts;
INSERT INTO punts (id, valor) VALUES (60,5);
BEGIN;
UPDATE punts SET valor = 4 WHERE id = 60;
SAVEPOINT a;
INSERT INTO punts (id, valor) VALUES (61,8);
SAVEPOINT b;
INSERT INTO punts (id, valor) VALUES (61,9);
ROLLBACK TO b;
COMMIT;
SELECT SUM(valor) FROM punts;
```
Primer fa dos transaccions no reversibles amb DELETE e INSERT.  
Després fa una transacció reversible amb UPDATE. A continuació crea un savepoint "a".  
A continuació fa una transacció INSERT i crea un altre savepoint "b".  
Per últim fa una transacció INSERT i retorna fins a la segona transacció INSERT. Fa definitius els cambis.