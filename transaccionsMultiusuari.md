**Jorge D. Pérez López**  
**a232508jp**  

# Transacciones (Multiusuari)

7. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor) VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) FROM punts; -- Connexió 2
```
L'usuar 0 fa dos transaccions que veurà tothom.  
L'usuari 1 comença una transacció, però només veu ell els canvis ja que no ha fet commit.  
L'usuari 2 veurà els canvis de l'usuari 0.  

8. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 4 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 81; -- Connexió 2

UPDATE punts SET valor = 10 WHERE id = 81; -- Connexió 1

UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 80; -- Connexió 0
```
L'usuari 0 fa dos transaccions.  
L'usuari 1 comença una transacció a la fila amb id 80.  
L'usuari 2 comença una transacció a la fila amb id 81.  
L'usuari 1 comença una transacció a la fila amb id 81, causant un bloqueig.  
L'usuari 2 comença una transacció a la fila amb id 80, causant un deadlock.  
A causa del deadlock, el sistema fa rollback de la transacció de l'usuari 2.  
L'usuari 1 finalitza la transacció i el SELECT mostra el valor 4.  

9. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2
INSERT INTO punts (id, valor) VALUES (91,9); -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 91; -- Connexió 0
```
L'usuari 0 fa una transacció visible per tothom.  
L'usuari 1 inicia una transacció i esborra tota la taula punts, només serà visible per ell.  
L'usuari 2 inicia una transacció que quedarà bloquejada ja que hi ha una operació que afecta a tota la taula pendent.  
L'usuari 1 finalitza la transacció i esborra la fila que ha inserit l'usuari 0.  
Els canvis de l'usuari 2 es guarden ja que s'ha desbloquejat la operació.  
El SELECT mostrarà el valor 9.   

10. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (100,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 100; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 100; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 100; -- Connexió 0
```
L'usuari 0 fa una transacció visible per tothom.  
L'usuari 1 inicia una transacció i canvïa el valor del id 100 a 6, només serà visible per ell.  
L'usuari 2 inicia una transacció sobre aquesta mateixa fila, quedant bloquejat.  
L'usuari 1 finalitza la transacció, desbloquejant a l'usuari 2. L'usuari 2 finalitza també la seva transacció.  
EL valor que és mostra és 7.   

11. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 110; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 111; -- Connexió 0
```
L'usuari 0 fa dos transaccions visibles per tothom.  
L'usuari 1 inicia una transacció sobre la fila amb id 110.  
L'usuari 2 inicia una transacció sobre les files amb id 110 i 111, quedant bloquejat. Craa un savepoint "a".  
L'usuari 1 finalitza la transacció, així que es desbloqueja la transacció de l'usuari 2.  
El valor que es mosra és 7.  


12. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts SET valor = 9 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 121; -- Connexió 0
```
L'usuari 0 fa dos transaccions visibles per tothom.  
