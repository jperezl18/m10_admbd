/* Mostrar usuari actual i role actual */
SELECT current_user, current_role;

/* Cambiar de role (només per la bd actual) */
SET ROLE /*rolename*/;

/* Resetejar els roles actuals */
SET ROLE none;
RESET ROLE;

/* Donar permisos a un role */
GRANT /*permís*/ ON /*objecte*/ TO /*user/role*/;

/* Treure permisos a un role */
REVOKE /*permís*/ ON /*objecte*/ FROM /*user/role*/;

/* Permissos :
    - SELECT
    - INSERT
    - UPDATE
    - DELETE */